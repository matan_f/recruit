<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dep=[['id' => 1, 'name' => 'HR','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['id' => 2, 'name' => 'Management','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")]];
        
        DB::table('departments')->insert($dep); 
    }
}
