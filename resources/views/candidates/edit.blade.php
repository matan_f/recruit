@extends('layouts.app')

@section('content')
<div class="jumbotron">
<h1 class="alert alert-primary">Edit candidate number: {{$id}}</h1>

<form method ="post" action = "{{action('CandidatesController@update',$candidate->id)}}">
@csrf
<input type="hidden" name="_method" value = "PATCH">
<div class="lead">
    <label for= "name">Candidate name</label>
    <input type="text" name="name" value = {{$candidate ->name}}>
</div>
<div class="lead">
    <label for= "email">Candidate email</label>
    <input type="text" name="email" value = {{$candidate ->email}}>
    
</div>
<div>
    <input class="btn btn-primary" type="submit" name="submit value" type="submit" name="submit value" value="Update Candidate">
</div>
</form>
</div>
        
@endsection

                 
           
    </body>
</html>
