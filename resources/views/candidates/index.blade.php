@extends('layouts.app')
@section('content')

<div><a href ="{{url('/candidates/create')}}">Add new candidate</a></div>

<h1 class="alert alert-success">List of candidates</h1>
<table class="table table-striped table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th><th>Edit</th>
    </tr>
    <!-- the table data -->
    @foreach($candidates as $candidate)
    <tr>
        <td>{{$candidate->id}}</td>
        <td>{{$candidate->name}}</td>
        <td>{{$candidate->email}}</td>
        <td>
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if(isset($candidate->user_id))
                    {{$candidate->owner->name}};
                @else
                Assign Owner
                @endif
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            @foreach($users as $user)
                <a class="dropdown-item" href="{{route('candidates.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
            @endforeach
            </div>
            </div>
        </td>
        <td>
            <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{$candidate->status->name}}
            </button>
            @if((App\status::next($candidate->status_id)) != null)
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(App\status::next($candidate->status_id) as $status)
                        <a class="dropdown-item" href="{{route('candidates.changestatus',[$candidate->id, $status->id])}}">{{$status->name}}</a>
                    @endforeach
                </div>
            @endif
            </div>
        </td>
        <td>{{$candidate->created_at}}</td>
        <td>{{$candidate->updated_at}}</td>
        <td><div><a href ="{{route('candidates.edit',$candidate->id)}}"> Edit</a></div></td>
        <td><div><a href ="{{route('candidates.delete',$candidate->id)}}"> Delete</a></div></td>
    </tr>
    
    @endforeach
</table>

@endsection                   
                    


                 
 